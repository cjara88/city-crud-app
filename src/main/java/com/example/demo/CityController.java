package com.example.demo;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cities")
public class CityController {
	
	@Autowired
	CityService cityService;
	
	@Autowired
	CityRepository cityRepository;
	
	@GetMapping
	List<City> findAll() {
		return cityRepository.findAll();
	}
	
	@GetMapping("{id}")
	Optional<City> getById(@PathVariable("id") Long id) {
		return cityRepository.findById(id);
	}
	
	@PostMapping
	City create(@RequestBody City city) {
		return cityRepository.save(city);
	}
	
	@PutMapping("{id}")
	City update(@RequestBody City newCity, @PathVariable Long id) {
		return cityRepository.findById(id).map(city -> {
			city.setName(newCity.getName());
			return cityRepository.save(city);
		}).orElse(null);
		
	}

	@DeleteMapping("/{id}")
	void delete(@PathVariable Long id) {
		cityRepository.deleteById(id);;
	}

}
