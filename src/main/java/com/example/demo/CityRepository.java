package com.example.demo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CityRepository extends JpaRepository<City, Long> {

	List<City> findByCountryId(Long idCountry);
	
	@Query("select c from City c where c.country.id = ?1")
	List<City> findByCountryIdWithQuery(Long idCountry);
	
	@Query("select c from City c where c.country.id = :idCountry")
	List<City> findByCountryIdWithQueryParamName(@Param("idCountry") Long idCountry);
	
	
	@Query(value = "SELECT * FROM city c WHERE c.country_id = ?1", nativeQuery = true)
	List<City> findByCountryIdWithNativeQuery(Long idQuery);
	
}
