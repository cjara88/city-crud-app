package com.example.demo;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/countries")
public class CountryController {
	
	@Autowired
	CountryRepository countryRepository;

	@Autowired
	CityRepository cityRepository;
	
	@GetMapping
	List<Country> findAll() {
		return countryRepository.findAll();
	}
	
	@GetMapping("{id}")
	Optional<Country> getById(@PathVariable("id") Long id) {
		return countryRepository.findById(id);
	}
	
	@GetMapping("/{id}/cities")
	List<City> findCitiesById(@PathVariable("id") Long id, @RequestParam(name = "tipoConsulta", defaultValue = "JPA") String tipoConsulta) {
		
		List<City> listCity = new ArrayList<>();
		
		switch (tipoConsulta) {
		case "JPQL":
			listCity = cityRepository.findByCountryIdWithQuery(id);
			break;
		case "JPQL_WITH_NAME_PARAM":
			listCity = cityRepository.findByCountryIdWithQueryParamName(id);
			break;
		case "NATIVE":
			listCity = cityRepository.findByCountryIdWithNativeQuery(id);
			break;
		case "JPA":
			listCity = cityRepository.findByCountryId(id);
			break;
		}
		
		return listCity;
	}
	
	@PostMapping
	Country create(@RequestBody Country country) {
		return countryRepository.save(country);
	}
	
	@PutMapping("{id}")
	Country update(@RequestBody Country newCountry, @PathVariable Long id) {
		return countryRepository.findById(id).map(country -> {
			country.setName(newCountry.getName());
			return countryRepository.save(country);
		}).orElse(null);
		
	}

	@DeleteMapping("/{id}")
	void delete(@PathVariable Long id) {
		countryRepository.deleteById(id);;
	}

}
